-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 20-Dez-2013 às 14:47
-- Versão do servidor: 5.1.71
-- versão do PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `bots_world`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `locales_achievement_reward`
--

CREATE TABLE IF NOT EXISTS `locales_achievement_reward` (
  `entry` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `subject_loc1` varchar(100) NOT NULL DEFAULT '',
  `subject_loc2` varchar(100) NOT NULL DEFAULT '',
  `subject_loc3` varchar(100) NOT NULL DEFAULT '',
  `subject_loc4` varchar(100) NOT NULL DEFAULT '',
  `subject_loc5` varchar(100) NOT NULL DEFAULT '',
  `subject_loc6` varchar(100) NOT NULL DEFAULT '',
  `subject_loc7` varchar(100) NOT NULL DEFAULT '',
  `subject_loc8` varchar(100) NOT NULL DEFAULT '',
  `text_loc1` text,
  `text_loc2` text,
  `text_loc3` text,
  `text_loc4` text,
  `text_loc5` text,
  `text_loc6` text,
  `text_loc7` text,
  `text_loc8` text,
  PRIMARY KEY (`entry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `locales_achievement_reward`
--

INSERT INTO `locales_achievement_reward` (`entry`, `subject_loc1`, `subject_loc2`, `subject_loc3`, `subject_loc4`, `subject_loc5`, `subject_loc6`, `subject_loc7`, `subject_loc8`, `text_loc1`, `text_loc2`, `text_loc3`, `text_loc4`, `text_loc5`, `text_loc6`, `text_loc7`, `text_loc8`) VALUES
(13, '', '', '', '', '', 'Nivel 80', '', '', NULL, NULL, NULL, NULL, NULL, 'Enhorabuena por tu convicción en alcanzar la 80ª etapa de tu aventura. No hay duda de que te has entregado a la limpieza de los males que asolan Azeroth.$B$BY aunque el camino hasta aquí no ha sido fácil, la auténtica batalla te espera aún.$B$B¡Sigue luchando!$B$BRhonin', NULL, NULL),
(45, '', '', '', '', '', '¡Has vivido aventuras!', '', '', NULL, NULL, NULL, NULL, NULL, '¡Mírate!$B$BY yo que pensaba que había visto cosas en este lugar helado. A este enano le parece obvio que el fuego del explorador brilla en tus ojos.$B$BViste con orgullo este tabardo, así tus amigos sabrán dónde pedir direcciones cuando llegue el momento.$B$B¡Sigue viajando!$B$BBrann Barbabronce', NULL, NULL),
(46, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(230, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(456, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(614, '', '', '', '', '', '¡Por la Alianza!', '', '', NULL, NULL, NULL, NULL, NULL, 'La guerra hace furor en nuestra tierra. Sólo los héroes más valientes osan pegar a la Horda donde más le duele. Tú eres uno de ellos.$B$BLos golpes que has asestado a los líderes de la Horda abren las puertas a nuestro ataque final. La Horda se inclinará ante la Alianza. $B$BTus logros serán recompensados. ¡Cabalga con orgullo!$B$B Tu rey', NULL, NULL),
(619, '', '', '', '', '', '¡Por la Horda!', '', '', NULL, NULL, NULL, NULL, NULL, 'En esta época de gran agitación, verdaderos héroes nacen de la miseria. Tu eres uno de esos grandes héroes.$B$BLa guerra nos recae a todos. Tus esfuerzos a nuestra causa en Azeroth y tus grandes hazañas se verán recompensados. Toma este premio de Orgrimmar y cabalga hacia la gloria.$B$B¡Por la Horda!$B$B- Jefe de Guerra Thrall', NULL, NULL),
(714, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(762, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(870, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(871, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(876, '', '', '', '', '', 'Brutalmente entregado', '', '', NULL, NULL, NULL, NULL, NULL, 'Te he estado observando, $gchico:chica;.$B$BTienes un gran talento para luchar en las arenas. Acepta este presente y llévalo con orgullo. ¡Ahora vuelve allí y muestrales cómo se hace!$B$BTío Sal', NULL, NULL),
(907, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(913, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(942, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(943, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(945, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(948, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(953, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(978, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1015, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1021, '', '', '', '', '', 'Por el Triunfador', '', '', NULL, NULL, NULL, NULL, NULL, 'No podía seguir ignorando la buena colección de tabardos que has conseguido reunir. No creo que sea inconveniente añadir uno más. Simplemente es que lleba acumulando polvo en mi armario durante mucho tiempo y no le doy utilidad.', NULL, NULL),
(1038, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1039, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1174, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1175, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1250, '', '', '', '', '', 'El nuevo hogar de Cuesqui', '', '', NULL, NULL, NULL, NULL, NULL, 'He oído que cuidas bien de nuestros peludos amigos. Necesito un nuevo hogar para Cuesqui. No se porta bien con los otros.$B$BNo olvides darle de comer dos veces al día. Ah, y tiene obsesión por los gatos negros.$B$B--Breanni', NULL, NULL),
(1400, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1402, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1516, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1563, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1656, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1657, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1658, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1681, '', '', '', '', '', 'Saludos desde Darnassus', '', '', NULL, NULL, NULL, NULL, NULL, 'Tus logros han sido profundos y de largo alcance. Azeroth, con todas las recientes turbulencias, se beneficia mucho con los que tratan de liberar la tierra de maldad.$B$BSólo los que le dedican tiempo a conocer nuestras tierras entienden los sacrificios de los caídos y el valor de nuestros héroes. Y tu eres uno de esos héroes. Es de esperar que se retracte en los cuentos tus aventuras y hazañas en los próximos años.$B$BEn nombre de la Alianza, te doy las gracias, Maestro cultural.', NULL, NULL),
(1682, '', '', '', '', '', 'Saludos desde Cima del Trueno', '', '', NULL, NULL, NULL, NULL, NULL, 'Las noticias de tus logros han llegado muy lejos. Los vientos susurran, a través de aullido de nuestras tierras, tus admirables hazañas. Los que están dispuestos a desafiar el mal son nuestra única esperanza.$B$BSólo aquellos que escuchan la voz de los vientos entienden las deudas que han pagado nuestros héroes caídos para proteger a nuestro pueblo. Espero que un héroe como tú pueda vivir mucho tiempo para contar los cuentos de tus aventuras, porque sólo así podremos recordar lo mucho que tenemos que estar agradecidos.$B$BNuestro más sincero agradecimiento, Maestro cultural.$B$B¡Por la Horda!$B$B- Cairne Pezuña de Sangre', NULL, NULL),
(1683, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1684, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1691, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1692, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1693, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1707, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1784, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(1793, '', '', '', '', '', '¡Gracias!', '', '', NULL, NULL, NULL, NULL, NULL, 'Estimado $N.$B$BGracias por sacarme durante la Semana del Niño. ¡Lady Liadrin, la líder de los Caballeros de Sangre, ha pedido que me dejen a su custodia para para entrenarme en la Ciudad Lunargenta!$B$BSólo quiero agradecerle por llevarme a ver a mi amigo, Hch''uu, así como al Portal Oscuro, al Trono de los Elementos y las Cavernas del Tiempo. ¡Me gusta mucho mi dragón de juguete!$B$BEstoy seguro de que llevas a mi mascota con cuidado. ¡Por favor, dile que lo echo de menos!$B$BAtentamente,$BSalandria.', NULL, NULL),
(1956, '', '', '', '', '', 'Conocimiento superior', '', '', NULL, NULL, NULL, NULL, NULL, 'Felicidades por completar tus estudios de Las Escuelas de la Magia Arcana. En reconocimiento a tu dedicación, te adjunto este volumen especial para completar la serie.$B$BEn este tomo encontrás de todo, entretenimiento, diversión, pero voy a dejar que lo descubras por tu $gmismo:misma;.$B$BAtentamente,$B$BRhonin', NULL, NULL),
(2051, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2054, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2096, '', '', '', '', '', 'El Maestro de las Monedas', '', '', NULL, NULL, NULL, NULL, NULL, '¡Saludos y felicitaciones por la recopilación de la serie completa de monedas de Dalaran! Como recompensa por todo tu duro trabajo hemos incluido una nueva moneda, el Sello de titanio de Dalaran. Se trata de una moneda especial que sólo se concede a los más ardientes defensores de los coleccionistas.$B$BEspero que disfrutes de este premio especial. ¡Te lo has ganado!$B$BAtentamente,$BJepetto Jugarreta', NULL, NULL),
(2136, '', '', '', '', '', 'La Gloria del Héroe', '', '', NULL, NULL, NULL, NULL, NULL, 'Campeón,$B$BLos susurros sobre las grandes hazañas que has realizado desde que llegastes a Rasganorte han llegado hasta el Templo del Reposo del Dragón.$B$BTu valor no debe pasar desapercibido. Por favor, acepta este regalo en nombre de los Aspectos. Juntos liberaremos el mal en Azeroth de una vez por todas.$B$BAlexstrasza la Protectora.', NULL, NULL),
(2143, '', '', '', '', '', 'Liderar la Caballería', '', '', NULL, NULL, NULL, NULL, NULL, 'No podía seguir ignorando lo bueno que eres con el ganado. Con toda la actividad que hay por aquí, el negocio ha ido mejor que nunca.$B$B¿Supongo que no te importará cuidar de este Draco Albino por mí? No tengo el tiempo suficiente para dedicarme a cuidar a todos estos animales.$B$B Atentamente,$BMei Francis', NULL, NULL),
(2144, '', '', '', '', '', 'Ha sido un largo y raro viaje', '', '', NULL, NULL, NULL, NULL, NULL, 'Con los tambores de guerra resonando lejos de aquí, es fácil para los habitantes de Azeroth olvidar todas las dificultades de la vida. Tú, en cambio, has mantenido la dignidad de las razas de Azeroth con tus admirables habilidades de lucha. Vamos a celebrar la victoria a lo grande, no como en las derrotas.$B$BRecuérdalo bien, juerguista. Otros pueden ser inspirados por tu buen humor,$B$BAlexstrasza la Protectora', NULL, NULL),
(2145, '', '', '', '', '', 'Ha sido un largo y raro viaje', '', '', NULL, NULL, NULL, NULL, NULL, 'Con los tambores de guerra resonando lejos de aquí, es fácil para los habitantes de Azeroth olvidar todas las dificultades de la vida. Tú, en cambio, has mantenido la dignidad de las razas de Azeroth con tus admirables habilidades de lucha. Vamos a celebrar la victoria a lo grande, no como en las derrotas.$B$BRecuérdalo bien, juerguista. Otros pueden ser inspirados por tu buen humor,$B$BAlexstrasza la Protectora', NULL, NULL),
(2186, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2187, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2188, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2336, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2516, '', '', '', '', '', 'Caza menor', '', '', NULL, NULL, NULL, NULL, NULL, '¡Hola!$B$BTengo entendido que has logrado dar a Cuesqui un hogar cálido y cariñoso. Tengo la esperanza de que puedas considerar la adopción de otro huérfano descarriado.$B$BEste cervatillo es muy tímido, pero no tendrás problemas para ganarte su amistad. ¡Con el adjunto su sal favorita!$B$B- Breanni', NULL, NULL),
(2536, '', '', '', '', '', 'Un montón de monturas', '', '', NULL, NULL, NULL, NULL, NULL, 'Tengo entendido que ahora tus establos están casi tan llenos como los míos. ¡Impresionante!$B$BTal vez podamos ayudarnos el uno al otro... Tengo dracohalcónes de sobra y espero que puedas darle a éste un nuevo hogar.$B$BNaturalmente se que ha sido entrenado como montura y no como una mascota de caza, aun así, te será como leal e incansable como cualquier otro caballo.$B$BLe saluda una vez más,$BMei', NULL, NULL),
(2537, '', '', '', '', '', 'Un montón de monturas', '', '', NULL, NULL, NULL, NULL, NULL, 'Tengo entendido que ahora tus establos están casi tan llenos como los míos. ¡Impresionante!$B$BTal vez podamos ayudarnos el uno al otro... Tengo dracohalcónes de sobra y espero que puedas darle a éste un nuevo hogar.$B$BNaturalmente se que ha sido entrenado como montura y no como una mascota de caza, aun así, te será como leal e incansable como cualquier otro caballo.$B$BLe saluda una vez más,$BMei', NULL, NULL),
(2760, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2761, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2762, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2763, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2764, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2765, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2766, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2767, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2768, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2769, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2797, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2798, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2816, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2817, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2903, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2904, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2957, '', '', '', '', '', 'La Gloria del Asaltante de Ulduar', '', '', NULL, NULL, NULL, NULL, NULL, 'Estimado $N, espero que estéis bien y que hayáis tenido tiempo suficiente para recuperaros de vuestras travesuras en Ulduar.$B$BMis muchachos del equipo de prospección encontraron a este pobre animal muerto ''cría de Draco''. Debe haber sido un experimento de los Enanos Férreos de la zona.$B$B¡Por suerte, hemos podido devolver a la vida el Draco y ahora mismo se encuentra perfectamente!$B$BNinguno de nosotros sabe mucho acerca de montar cualquier cosa menos de carneros y mulas de carga y ya que os debíamos un favor por lo que habéis hecho allí... Pensamos que tal vez lo aceptarías como regalo.$B$BAtentamente,$BBrann Barbabronce.', NULL, NULL),
(2958, '', '', '', '', '', 'La Gloria del Asaltante de Ulduar', '', '', NULL, NULL, NULL, NULL, NULL, 'Estimado $N, espero que estéis bien y que hayáis tenido tiempo suficiente para recuperaros de vuestras travesuras en Ulduar.$B$BMis muchachos del equipo de prospección encontraron a este pobre animal muerto ''cría de Draco''. Debe haber sido un experimento de los Enanos Férreos de la zona.$B$B¡Por suerte, hemos podido devolver a la vida el Draco y ahora mismo se encuentra perfectamente!$B$BNinguno de nosotros sabe mucho acerca de montar cualquier cosa menos de carneros y mulas de carga y ya que os debíamos un favor por lo que habéis hecho allí... Pensamos que tal vez lo aceptarías como regalo.$B$BAtentamente,$BBrann Barbabronce.', NULL, NULL),
(3036, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3037, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3117, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3259, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3316, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3478, '', '', '', '', '', 'Un pavo sin engullir', '', '', NULL, NULL, NULL, NULL, NULL, '¿Puedes creer que este pavo cebado ha logrado sobrevivir todo noviembre?$B$BDado que todos sus amigos fueron servidos en las mesas Generosas junto con el Chatni de arándanos y el Relleno de pan y... ooo ... Estoy ambriento. De todos modos, ahora está solo y me preguntaba si estarías dispuesto a cuidar de él, ya que no dispongo de espacio suficiente en la tienda.$B$BTan solo debes mantenerlo alejado del fuego de la cocina. Se le pone una mirada extraña cuando se siente alreredor de ello...', NULL, NULL),
(3656, '', '', '', '', '', 'Un pavo sin engullir', '', '', NULL, NULL, NULL, NULL, NULL, '¿Puedes creer que este pavo cebado ha logrado sobrevivir todo noviembre?$B$BDado que todos sus amigos fueron servidos en las mesas Generosas junto con el Chatni de arándanos y el Relleno de pan y... ooo ... Estoy ambriento. De todos modos, ahora está solo y me preguntaba si estarías dispuesto a cuidar de él, ya que no dispongo de espacio suficiente en la tienda.$B$BTan solo debes mantenerlo alejado del fuego de la cocina. Se le pone una mirada extraña cuando se siente alreredor de ello...', NULL, NULL),
(3857, '', '', '', '', '', 'Maestro de la Isla de la Conquista', '', '', NULL, NULL, NULL, NULL, NULL, 'Honorable $N,$B$BPor tus actos en la Isla de la Conquista,$Btengo el honor de entregarte este$Btabardo. Llévalo con orgullo.$B$BAlto comandante Halford Aterravermis', NULL, NULL),
(3957, '', '', '', '', '', 'Maestro de la Isla de la Conquista', '', '', NULL, NULL, NULL, NULL, NULL, 'Honorable $N,$B$BPor tus actos en la Isla de la Conquista,$Btengo el honor de entregarte este$Btabardo. Llévalo con orgullo.$B$BAlto comandante Halford Aterravermis', NULL, NULL),
(4078, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4079, '', '', '', '', '', 'Un tributo a la inmortalidad', '', '', NULL, NULL, NULL, NULL, NULL, 'Estimad$go:a; $N.$B$BHistorias de tu reciente actuación en la prueba del Gran Cruzado se contarán, una y otra vez, en los siglos venideros. Cuando la Cruzada Argenta emitida su llamamiento a los más grandes campeones de Azeroth para poner a prueba su temple en la encrucijada del Coliseo. Tengo absoluta esperanza de que señales de la luz como tu y tus compañeros surjan de la batalla.$B$BNecesitaremos de tu destreza en la próxima batalla contra el Rey Exánime. Pero este día, regocijamos y celebramos tus gloriosos logros, y acepta este presente, es uno de nuestros caballos de guerra más finos. Cuando la plaga vea surgir por el horizonte tu estandarte. Hero$go:ina;, ¡su final está acerca!.$B$BTuyo con honor.$BTirion Fordring', NULL, NULL),
(4080, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4156, '', '', '', '', '', 'Un tributo a la inmortalidad', '', '', NULL, NULL, NULL, NULL, NULL, 'Estimad$go:a; $N.$B$BHistorias de tu reciente actuación en la prueba del Gran Cruzado se contarán, una y otra vez, en los siglos venideros. Cuando la Cruzada Argenta emitida su llamamiento a los más grandes campeones de Azeroth para poner a prueba su temple en la encrucijada del Coliseo. Tengo absoluta esperanza de que señales de la luz como tu y tus compañeros surjan de la batalla.$B$BNecesitaremos de tu destreza en la próxima batalla contra el Rey Exánime. Pero este día, regocijamos y celebramos tus gloriosos logros, y acepta este presente, es uno de nuestros caballos de guerra más finos. Cuando la plaga vea surgir por el horizonte tu estandarte. Hero$go:ina;, ¡su final está acerca!.$B$BTuyo con honor.$BTirion Fordring', NULL, NULL),
(4477, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4478, '', '', '', '', '', 'Buscar muchísimo más', '', '', NULL, NULL, NULL, NULL, NULL, '$gQuerido solitario:Querida solitaria;,$B$B Queremos reconocer tu tenacidad por recorrer mazmorras con gente a la que no conoces. Con un poco de suerte, les habrás enseñado a los novatos cosas nuevas.$B$BSeremos breves: hemos oído que te gustan los grupos aleatorios. Y como nunca sabes qué te va a tocar, te regalamos un perrito para que te haga compañía. O algo.$B$BAbrazos,$B$BTus amigos del equipo de Desarrollo de WoW', NULL, NULL),
(4530, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4583, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4584, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4597, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4598, '', '', '', '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4602, '', '', '', '', '', 'La gloria del asaltante de Corona de Hielo (10 j.)', '', '', NULL, NULL, NULL, NULL, NULL, '$N.$B$BCuando la influencia del Rey Exánime decae, algunos de sus esbirros más poderosos, se liberan de sus ataduras.$B$B Esta drako vermis de escarcha capturada por mis hombres en un buen ejemplo. Tiene algo más que voluntad propia.$B$B Uno de mis hombres perdió un brazo sesgado por ella, pero se lleva bien con sus jinetes - siempre y cuando sean hábiles y decididos.$B$BPor favor acepta esta magnífica bestia como presente de los Caballeros de la Espada de Ébano. Fue un honor pelear a tu lado en la más grande de las batallas.$B$BCon honor,$BDarion Mograine', NULL, NULL),
(4603, '', '', '', '', '', 'La gloria del asaltante de Corona de Hielo (25 j.)', '', '', NULL, NULL, NULL, NULL, NULL, '$N.$B$BCuando la influencia del Rey Exánime decae, algunos de sus esbirros más poderosos, se liberan de sus ataduras.$B$B Esta drako vermis de escarcha capturada por mis hombres en un buen ejemplo. Tiene algo más que voluntad propia.$B$B Uno de mis hombres perdió un brazo sesgado por ella, pero se lleva bien con sus jinetes - siempre y cuando sean hábiles y decididos.$B$BPor favor acepta esta magnífica bestia como presente de los Caballeros de la Espada de Ébano. Fue un honor pelear a tu lado en la más grande de las batallas.$B$BCon honor,$BDarion Mograine', NULL, NULL),
(4784, '', '', '', '', '', 'Intendentes de emblemas en Enclave de plata de Dalaran', '', '', NULL, NULL, NULL, NULL, NULL, 'Tus logros en Rasganorte no han pasado desapercibidos, amigo. $B$BLos emblemas que has ganado pueden usarse para comprar equipo de los diferentes Intendentes de emblemas en Dalaran. $B$BPuedes encontrarlos allí, en el Enclave de plata de Dalaran, donde cada variedad de emblemas tiene su propio intendente. $B$B¡Esperamos tu llegada!', NULL, NULL),
(4785, '', '', '', '', '', 'Intendentes de emblemas en el Santuario Atracasol de Dalaran', '', '', NULL, NULL, NULL, NULL, NULL, 'Tus logros en Rasganorte no han pasado desapercibidos, amigo. $B$BLos emblemas que han ganado pueden usarse para comprar equipo de los diferentes Intendentes de emblemas en Dalaran. $B$BPuedes encontrarlos allí, en el Santuario Atracasol, donde cada variedad de emblemas tiene su propio intendente. $B$B¡Esperamos tu llegada!', NULL, NULL);
